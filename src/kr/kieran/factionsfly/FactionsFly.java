package kr.kieran.factionsfly;

import kr.kieran.factionsfly.commands.FlyCommand;
import kr.kieran.factionsfly.commands.StealthCommand;
import kr.kieran.factionsfly.commands.UtilCommand;
import kr.kieran.factionsfly.listeners.PlayerListeners;
import kr.kieran.factionsfly.managers.Managers;
import kr.kieran.factionsfly.utilities.Authentication;
import kr.kieran.factionsfly.utilities.Checks;
import kr.kieran.factionsfly.utilities.Messages;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class FactionsFly extends JavaPlugin {

    private static FactionsFly instance;
    private Managers managers;
    private Checks checks;

    @Override
    public void onEnable() {
        instance = this;
        ConsoleCommandSender commandSender = Bukkit.getServer().getConsoleSender();
        commandSender.sendMessage("§a§m--------------[-§r§a §aFactions Fly §m-]--------------");

        getConfig().options().copyDefaults(true);
        saveConfig();

        registerCommands();
        registerListeners();
        registerManagers();

        if (getServer().getPluginManager().getPlugin("Factions") != null) {
            commandSender.sendMessage("§aFactions found, hooking into Factions...");
        } else {
            commandSender.sendMessage("§cError: Factions could not be found, disabling...");
            getServer().getPluginManager().disablePlugin(this);
        }

        checks.checkPlayersRunnable();

        commandSender.sendMessage("§2FactionsFly by Kieraaaan has been enabled.");
        commandSender.sendMessage("§a§m--------------[-§r§a §aFactions Fly §m-]--------------");
    }

    @Override
    public void onDisable() {
        ConsoleCommandSender commandSender = Bukkit.getServer().getConsoleSender();
        commandSender.sendMessage("§c§m--------------[-§r§c Factions Fly §m-]--------------");
        managers.onDisable();
        getServer().getScheduler().cancelTasks(this);
        saveConfig();
        commandSender.sendMessage("§4FactionsFly by Kieraaaan has been disabled.");
        commandSender.sendMessage("§c§m--------------[-§r§c Factions Fly §m-]--------------\n");
        instance = null;
    }

    private void registerCommands() {
        getCommand("fly").setExecutor(new FlyCommand());
        getCommand("flyadmin").setExecutor(new UtilCommand());
    }

    private void registerListeners() {
        PluginManager pluginManager = getServer().getPluginManager();
        pluginManager.registerEvents(new StealthCommand(), this);
        pluginManager.registerEvents(new PlayerListeners(), this);
    }

    private void registerManagers() {
        managers = new Managers();
        checks = new Checks();
    }

    public Managers getManagers() {
        return managers;
    }

    public Checks getChecks() {
        return checks;
    }

    public static FactionsFly getInstance() {
        return instance;
    }
}
