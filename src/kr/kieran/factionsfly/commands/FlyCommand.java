package kr.kieran.factionsfly.commands;

import kr.kieran.factionsfly.FactionsFly;
import kr.kieran.factionsfly.utilities.Messages;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FlyCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!Messages.FLY_ENABLED && sender.hasPermission("ffx.admin")) {
            sender.sendMessage(Messages.FLIGHT_TEMPORARILY_DISABLED);
            return true;
        }

        if (!(sender instanceof Player)) {
            sender.sendMessage(Messages.NOT_A_PLAYER);
            return true;
        }

        Player player = (Player) sender;

        if (!player.hasPermission("ffx.user")) {
            player.sendMessage(Messages.NO_PERMISSION);
            return true;
        }

        if (args.length == 0) {
            if (FactionsFly.getInstance().getChecks().playerInAllowedLand(player)) {

                if (FactionsFly.getInstance().getChecks().enemyPlayerNear(player)) {
                    player.sendMessage(Messages.ENEMY_NEARBY);
                    return true;
                }

                if (!FactionsFly.getInstance().getManagers().canFly(player)) {
                    FactionsFly.getInstance().getManagers().addFly(player);
                    player.sendMessage(Messages.FLIGHT_ENABLED);
                    return true;
                }

                FactionsFly.getInstance().getManagers().removeFly(player);
                player.sendMessage(Messages.FLIGHT_DISABLED);
            } else {
                player.sendMessage(Messages.DISALLOWED_LAND);
            }
        }

        return true;
    }

}
