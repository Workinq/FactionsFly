package kr.kieran.factionsfly.listeners;

import kr.kieran.factionsfly.FactionsFly;
import kr.kieran.factionsfly.utilities.Messages;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.*;

public class PlayerListeners implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        if (!Messages.FLY_ENABLED) return;
        Player player = event.getPlayer();
        if (FactionsFly.getInstance().getChecks().playerInAllowedLand(player)) {
            player.sendMessage(Messages.AUTO_FLIGHT);
            FactionsFly.getInstance().getManagers().addFly(player);
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        if (!Messages.FLY_ENABLED) return;
        Player player = event.getPlayer();
        FactionsFly.getInstance().getManagers().removeFly(player);
    }

    @EventHandler
    public void onPlayerKick(PlayerKickEvent event) {
        if (!Messages.FLY_ENABLED) return;
        Player player = event.getPlayer();
        FactionsFly.getInstance().getManagers().removeFly(player);
    }

    @EventHandler
    public void onPlayerPearl(PlayerInteractEvent event) {
        if (!Messages.FLY_ENABLED) return;
        if (!Messages.ENDERPEARL_CHECK) return;
        Player player = event.getPlayer();
        if (!player.isFlying()) return;
        if (event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
            if (event.getMaterial() == Material.ENDER_PEARL) {
                event.setUseItemInHand(Event.Result.DENY);
                player.sendMessage(Messages.ENDERPEARL);
            }
        }
    }

    @EventHandler
    public void onUpdateLand(PlayerMoveEvent event) {
        if (!Messages.FLY_ENABLED) return;
        Player player = event.getPlayer();
        if (player.getGameMode() != GameMode.SURVIVAL
                || player.hasPermission("ffx.admin")
                || player.hasPermission("ffx.all")) {
            return;
        }
        if (!FactionsFly.getInstance().getChecks().playerInAllowedLand(player)) {
            FactionsFly.getInstance().getManagers().countdownRemoveFly(player, "land");
        } else if (FactionsFly.getInstance().getChecks().enemyPlayerNear(player)) {
            FactionsFly.getInstance().getManagers().countdownRemoveFly(player, "enemy");
        } else {
            FactionsFly.getInstance().getManagers().addFly(player);
        }
    }

    @EventHandler
    @SuppressWarnings("deprecation")
    public void onPlayerFlightDamage(PlayerMoveEvent event) {
        if (!Messages.FLY_ENABLED) return;
        Player player = event.getPlayer();
        if (FactionsFly.getInstance().getManagers().hasNoFall(player) && player.isOnGround()) {
            FactionsFly.getInstance().getManagers().removeNoFall(player);
        }
    }

    @EventHandler
    public void onTakeDamage(EntityDamageEvent event) {
        if (!Messages.FLY_ENABLED) return;
        if (!(event.getEntity() instanceof Player)) return;
        Player player = (Player) event.getEntity();
        if (event.getCause() == EntityDamageEvent.DamageCause.FALL
                && FactionsFly.getInstance().getManagers().hasNoFall(player)) {
            event.setCancelled(true);
            FactionsFly.getInstance().getManagers().removeNoFall(player);
        }
    }

}
